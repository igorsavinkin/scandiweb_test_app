<?php 
session_start();
if (isset($_GET["csrf"]) && $_GET["csrf"] == $_SESSION["token"]){  
	include 'autoloader.php';
	$data = $info ='';
	
	// delete products if needed
	if (isset($_GET["delete"]) && isset($_GET["ids"]) ){		 
		$info = product::deleteByIds($_GET["ids"]);
	}
	// return all existing products
	if (isset($_GET["load"])){
		$data = product::getAllProducts();  
	}	
	// return object as json to client
	echo json_encode(["products"=>$data, "info"=>$info]) ;
} else {
	echo 'CSRF token mismatch'; 
}  