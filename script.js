function openTab(evt, tabName, crsf_token='') {
	// Declare all variables
	var i, tabcontent, tablinks;

	// Get all elements with class="tabcontent" and hide them
	tabcontent = document.getElementsByClassName("tabcontent");
	for (i = 0; i < tabcontent.length; i++) {
		tabcontent[i].style.display = "none";
	}

	// Get all elements with class="tablinks" and remove the class "active"
	tablinks = document.getElementsByClassName("tablinks");
	for (i = 0; i < tablinks.length; i++) {
		tablinks[i].className = tablinks[i].className.replace(" active", "");
	}

	// Show the current tab, and add an "active" class to the button that opened the tab
	document.getElementById(tabName).style.display = "block";
	evt.currentTarget.className += " active";
  
	if (tabName=='productList'){
		loadProducts(crsf_token);
	}
}

function product_attribute() {
	var product_type = document.getElementById("product_type").value; 
	// hide all the subforms
	var subforms = document.getElementsByClassName("subform");
	for(var i = 0; i < subforms.length; i++){
		subforms.item(i).style.display = "none";
	}
	// show a chosen subform
	document.getElementById(product_type).style.display = "block";   
};


function validateForm() { 
	event.preventDefault();
	let form = document.forms["add_form"];
	let sku = form["SKU"].value; 
	let xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			if (this.responseText == 'ok'){ 
				form.submit();
				// click to the Product Add tab
				document.getElementById("addProduct").click();
			} else {
				form["SKU"].setCustomValidity(this.responseText);
				return false;
			}
		}
	};
	xhttp.open("GET", "check_sku.php?csrf="+form.csrf.value+"&sku="+sku, true);
	xhttp.send();	
} 

function loadProducts(csrf_token) {
	let products_div = document.getElementById('allProducts'); 
	let xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			if (this.responseText != 'token mismatch'){ 				
				// load products from the server as json
				let products = JSON.parse(this.responseText).products; 
				displayProducts(products);
			} else { 
				return false;
			}
		}
	};
	xhttp.open("GET", "service.php?load=1&csrf="+csrf_token, true);
	xhttp.send();	
}

function displayProducts(products){
	if (products) {
		// clear up all previous products in the grid
		document.getElementById('pure-grid').innerHTML=''; 
		for (var key in products) {
			if (products.hasOwnProperty(key)) { 
				let id = products[key].id;
				let SKU = products[key].SKU;
				let name = products[key].name;
				let price = products[key].price; 
				let note;
				switch (products[key]._type) {
					case 'book':
						note = 'Weight ' + products[key].note + ' kg.';							
						break;
					case 'disk':
						note =  products[key].note + ' MB';	
						break;
					case 'furni':
						note = 'Dimensions ' + products[key].note;	
						break;
				}						 
				let div = document.createElement("div");
				div.setAttribute('class', "pure-u-1-5 product-box");
				div.setAttribute('id', 'pr-' + id );
				div.setAttribute('name', name);
				div.innerHTML = `<input class="checkbox" type="checkbox" >
				<div style="text-align:center;">${SKU}<br /><b>${name}</b><br />$ ${price}<br />${note}</div>`;

				document.getElementById("pure-grid").appendChild(div);	
			}
		}
	} else {
		document.getElementById("pure-grid").style.letterSpacing = "0.12em";
		document.getElementById("pure-grid").innerHTML='No products in DB now!'; 
	}
}
function deleteByIds(ids_string, csrf_token){
	let xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {   
			if (this.responseText != 'token mismatch'){ 
				let result = JSON.parse(this.responseText);  
				displayProducts(result.products);
				// alert(result.info); 
				return true;
			} else { 
				alert('Failure to delete products');
				return false;
			}
		}
	};
	xhttp.open("GET", "service.php?load=1&delete=1&ids="+ids_string+"&csrf="+csrf_token, true);
	xhttp.send();
}
function applyMultiple(){
	let actions = document.getElementById("actions"); 
	let option_selected = actions.options[actions.selectedIndex].value; 
	// get all products' checkboxes
	var checkboxes = document.getElementsByClassName("checkbox"); 
	switch (option_selected) {
		case '1': 
			for(let checkbx of checkboxes){
			    checkbx.checked = true; 
			} 
			break;
		case '2': 
			for(let checkbx of checkboxes){
			    checkbx.checked = false; 
			} 	
			break;
		case '3':
			let ids = ''; 
			let pr_names='';
			for(let checkbx of checkboxes){  // gather checked checkboxes
				if (checkbx.checked == true){					
					ids += checkbx.parentNode.id.split("-")[1] + ',';
					pr_names += checkbx.parentNode.attributes.name.nodeValue +'\n'; 
				}				
			} 
			ids = ids.slice(0, -1); // remove last comma
			console.log('ids:', ids);
			if (ids){
				let _delete = confirm("Do you really want to DELETE the following items:\n"+pr_names+'?');
				if (_delete == true) {
					// get 'csrf_token' to use in the 'deleteByIds' function			    
					let csrf_token = document.querySelector("input[id=_csrf]").value; 
					deleteByIds(ids, csrf_token); 
				} 
			}
			break;
	}
}