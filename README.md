# Scandiweb Senior Test App

## Intro
The following PHP app operates with product items (`product` DB table) and allows the following operations:
 
 1. Add product
 2. View all products in DB
 3. Select multiple (all) products at the Product tab
 4. Unselect all products
 5. Delete selected products

## App components
The app was developed at PHP language with the use of JS and [Pure CSS library](https://purecss.io/).

`db.php` file is a utility script facilitating the DB input/output using the prepared statement.

## Intallation
Clone the project into your PHP enviroment and set up the `db_config.php` with appropriate DB credentials.

The products DB table is called `product`. 

After you set up DB, please execute the content of the `db_create.sql` file for the creating the `product` table.

## Run 
Run the `index.php` file from the project folder.