<?php session_start(); 
$_SESSION["token"] = md5(uniqid(mt_rand(), true)); 
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
	<meta name="_csrf" th:content=""/>
    <title>Scandiweb Product App</title>
	<link href="style.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="https://unpkg.com/purecss@1.0.1/build/pure-min.css" integrity="sha384-oAOxQR6DkCoMliIh8yFnu25d7Eq/PHS21PClpwjOTeU2jRSq11vu66rf90/cZr47" crossorigin="anonymous">
	<script src='script.js' type="text/javascript"></script> 
	
</head>
<body>
    <?php if (isset($_SESSION["flash"])) {  
		echo $_SESSION["flash"];
		unset($_SESSION["flash"]);		
	} ?>
	<h2>Scandiweb Product App</h2>
	<div class="tab">
	  <button class="tablinks pure-button" onclick="openTab(event, 'productList', '<?php echo $_SESSION['token']; ?>')"  id="defaultOpen" >Product List</button>
	  <button class="tablinks pure-button" onclick="openTab(event, 'addProduct')" 
	     id="addProductTab" >Product Add</button> 
	</div>

	<!-- Tab content --> 
	<div id='addProduct' class="tabcontent" >	   
		<h3>Product Add</h3>
		<form action="add_product.php" method="post" name="add_form" class="pure-form pure-form-stacked" onsubmit="return validateForm()">
			<div class="pure-g">
				<div class="pure-u-1-2">
				<input type="text" placeholder="SKU" id="SKU" name="SKU" required unique ><br />
				<input type="text" placeholder="Name" id="name" name="name" required unique ><br />
				<input type='number' min="0.00" step="0.01" max="10000000" name="price" id="price" placeholder="Price" required ><!-- pattern='[0-9]+(\.[0-9][0-9]?)?' -->
				</div>
				<div class="pure-u-1-2">
					Select a product type:<br />
					<select id='product_type' onchange="product_attribute()" name='product_type'>
					  <option value="disk">Disk</option>
					  <option value="book">Book</option>
					  <option value="furni">Furniture</option> 
					</select>
					<span id="disk" class='subform' >
						<input type='number' pattern='[0-9]+'  name="size" id="size" placeholder="Size" ><br /> 
						Enter the size of Disk in MB.
					</span>
					<span id="book" class='subform' style="display:none">
						<input type='number' min="0.00" step="0.01" max="10000000" name="book_weight" id="book_weight" placeholder="Weight"  ><br /> 
						Enter book weight in KG.
					</span>
					<span id="furni" class='subform' style="display:none">
						<input type='number' pattern='[0-9]+' name="height" id="height" placeholder="Height" ><br /> 
						<input type='number' pattern='[0-9]+' name="width" id="width" placeholder="Width" ><br />
						<input type='number' pattern='[0-9]+' name="length" id="length" placeholder="Length" ><br />
						Please provide dimensions in HxWxL format.
					</span>
				</div>
			</div> 
			<!-- regex for price validation https://stackoverflow.com/a/8211050/1230477 -->
			
			<input type="hidden" id="_csrf" name="csrf" value="<?php echo $_SESSION["token"]; ?>" />
			<p><input type="submit" name="submitBtn" value="Add product" class="pure-button pure-button-primary" /></p>  	 
		</form>
	</div>

	<div id="productList" class="tabcontent" >
		<h3>Product List</h3>
		<div style="text-align:right" class="pure-form">
			<select id="actions">				
				<option value=1>Select all products</option>
				<option value=2>Unselect all products</option>
				<option value=3>Delete multiple products</option>
			</select>
			<button type="text" class="pure-button pure-button-primary" onclick="applyMultiple()">Apply</button>
		<div>
		<div id="products">
			<div class="pure-g" id="pure-grid"> 
			</div>
		</div>
	</div>
</body>
<script type="text/javascript">window.onload=function(){
	// select init tab <!--based on the request done-->
	let tab_id = "defaultOpen";  <!--?php if ($_POST){ echo '"addProductTab";';	} else { echo '"defaultOpen";'; } ?-->
	document.getElementById(tab_id).click();

	// adding addEventListener to the 'SKU' input to remove CustomValidity on input 
	document.getElementById("SKU").addEventListener("input", function (){
		this.setCustomValidity(''); 
	}); 
}</script>