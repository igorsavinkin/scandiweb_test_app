DROP TABLE IF EXISTS `product`;
CREATE TABLE `product` (
 `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
 `name` varchar(64) NOT NULL,
 `price` decimal(10,2) NOT NULL,
 `SKU` varchar(32) NOT NULL,
 `_type` varchar(5) NOT NULL COMMENT 'book, disk, furni',
 `note` varchar(32) NOT NULL,
 PRIMARY KEY (`id`),
 UNIQUE KEY `sku` (`SKU`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Products table for the scandiweb app'
