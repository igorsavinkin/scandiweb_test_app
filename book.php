<?php
class Book extends Product {
	function __construct(){
		parent::__construct();
		$this->set_Type();
	}	
	protected function set_Type()   
    {
        $this->type = 'book'; 
    }
}