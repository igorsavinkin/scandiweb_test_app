<?php
/**
 * `Product` abstract class    
 */ 
abstract class Product {
	
	protected $id;
	protected $name;
	protected $price;
	protected $sku;
	protected $type;	
	protected $note;  
	
	function __construct() {
        $this->id = '';
		$this->price = '';
		$this->name = '';
		$this->sku = '';
		$this->type = '';
		$this->note = '';  
    } 
	static function getDbPointer(){  // utility function
		require_once 'db_config.php';
		return new db(DB_HOST, DB_USER, DB_PASS, DB_NAME); 
	}
	static function check_sku($sku){  // used with XHR calls only
		$res = self::getDbPointer()->query("SELECT * FROM product WHERE SKU = ? ", htmlentities( $sku, ENT_QUOTES) )->fetchArray();     
		echo $res ? 'The SKU value is already in the product DB.' : 'ok';
		exit();
	} 
	static function deleteById($db, $id){		
		$query = $db->query("DELETE FROM product WHERE id=?", $id) ;     
		$res = $query->affectedRows(); 
		return $res ? 1 : 0;
	}
	static function deleteByIds($ids){
		$counter=0;
		$db = self::getDbPointer();
		foreach(array_filter(explode(',', $ids)) as $id){			
			$counter += self::deleteById($db, intval($id));
		} 
		return 'Deleted ' . $counter . ' product(s).'; 
	}
	
	static function getAllProducts(){
		$res = self::getDbPointer()->query("SELECT * FROM product")->fetchAll();     
		return $res ? $res : false; 		
	}
	public function saveToDB() { 		
		try {
			$db = self::getDbPointer();
			$res = $db->query('INSERT INTO product (SKU, name, price, _type, note) VALUES (?,?,?,?,?)', 
			$this->getSKU(), $this->getName(), $this->getPrice() , $this->get_Type() , $this->getNote() ); 
			if (is_string($res) && strpos($res, 'Unable to process MySQL query') !== false ){
				//header("Location " . $_SERVER['REQUEST_URI']); //  redirect with GET.
				return [false, $res];
			} else {
				$rows = $res->affectedRows();		 
				if ($rows) {
					$output = 'Product is succesfully added!';
					$return = true;
				} else {
					$output = 'Failure to add a product!';
					$return = false;
				}
				return [$return, $output];
			}			
		} catch (Exception $e) {
			$output_negative = 'Error: '. $e->getMessage(); 
			return [false, 'Error: '. $e->getMessage()];
		}
    }
		
	// getters
	public function getId()   
    {
        return $this->id;
    }
	public function getName()   
    {
        return $this->name;
    }
	public function getSKU()   
    {
        return $this->sku;
    }
	public function getPrice()   
    {
        return $this->price;
    }
	public function get_Type()   
    {
        return $this->type;
    }
	public function getNote()
	{
		return $this->note;
	}
	// setters
	public function setName($name)   
    {
        $this->name = $name; 
    }
	public function setPrice($price)   
    {
        $this->price = $price;
    }
	abstract protected function set_Type(); // to be implemented in children classes
	
	public function setSKU($sku)   
    {
        $this->sku = $sku;
    }
	public function setNote($note)   
    {
        $this->note = $note;
    }
}