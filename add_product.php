<?php 
session_start();
include 'autoloader.php';  
if (isset($_POST) && isset($_POST['product_type']) &&
    isset($_POST["csrf"]) && $_POST["csrf"] == $_SESSION["token"]){  // save a product  	
	switch ($_POST['product_type']) {
		case 'disk':
			$product = new Disk();
			$product->setNote( htmlentities($_POST['size'], ENT_QUOTES));
			break;
		case 'book':
			$product = new Book();
			$product->setNote( htmlentities($_POST['book_weight'], ENT_QUOTES)); 
			break;
		case 'furni': 
			$product = new Furniture();
			$product->setNote( htmlentities(implode("x", array($_POST['width'], $_POST['height'], $_POST['length'])), ENT_QUOTES));
	}
	$product->setPrice(htmlentities( $_POST['price'], ENT_QUOTES));	 
	$product->setName( htmlentities( $_POST['name'], ENT_QUOTES));
	$product->setSKU(  htmlentities( $_POST['SKU'], ENT_QUOTES)); 
	// saving product to DB
	$res = $product->saveToDB();
	if($res[0]){
		$flash_msg= '<p class="success flash">'. $res[1] . '</p>';
	} else {
		$flash_msg= '<p class="error flash">'. $res[1] . '</p>';  	
	}
} else {
	$flash_msg = '<p class="error flash">CSRF token mismatch</p>';
}
$_SESSION["flash"] = $flash_msg;
header("Location: index.php"); 
?>